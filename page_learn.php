<?php
/*
 * Template Name: Learn
 */

get_header();
?>
    <article id="learn">
        <h2 id="page-title"><?php the_title(); ?></h2>
        <?php the_content(); ?>
    </article>
    <article id="blog">
        <h3><a href="https://blog.spsdgtl.com/"><?php _e('Blog', 'spsdgtl'); ?></a></h3>
        <p>Engage with our growing network of thought leaders to help guide your digital decisions.</p>
        <section id="blog-posts">
            <a class="blog-post" href="https://blog.spsdgtl.com/new-webinar-series" rel="bookmark">
                <img src="https://blog.spsdgtl.com/hubfs/camera_featured.jpg?t=1516300458116">
                <h4 class="title">3 Reasons Why You Can’t Miss Our New Webinar Series</h4>
            </a>
            <a class="blog-post" href="https://blog.spsdgtl.com/facebook-news-feed-update" rel="bookmark">
                <img src="https://blog.spsdgtl.com/hubfs/three-handed-monster-resized.png?t=1516300458116">
                <h4 class="title">The Facebook News Feed Update: What to Focus on Now</h4>
            </a>
            <a class="blog-post" href="https://blog.spsdgtl.com/digital-communications-in-mexico" rel="bookmark">
                <img src="https://blog.spsdgtl.com/hubfs/mexicoplane-027231-edited.jpg?t=1516300458116">
                <h4 class="title">Why WhatsApp Could Be the Answer to Digital Communications in Mexico</h4>
            </a>
        </section>
        <a href="https://blog.spsdgtl.com/" class="cta arrow alt">View More</a>
    </article>
    <article id="events">
        <h3>Events</h3>
        <p>Connect with our thought leaders IRL. Check out our upcoming events below:</p>
        <a href="https://www.eventbrite.com/e/keeping-social-media-social-with-martin-lieberman-tickets-39653199788">Keeping Social Media Social with Martin Lieberman</a> - Feb 21, 2018
    </article>
    <article id="webinars">
        <h3>Webinars</h3>
        <p>Coming soon in 2018!</p>
    </article>
    <article id="newsletter-signup">
        <h2>Receive our newsletter!</h2>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <div id="newsletter-signup-inputs">
            <script>
                hbspt.forms.create({
                    portalId: '4063536',
                    formId: 'c36f0ae8-8732-460c-a1c1-54815e29d7ed',
                    css: ''
                });
            </script>
        </div>
    </article>
<?php
get_sidebar( 'newsletter' );
get_footer();